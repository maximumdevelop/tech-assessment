<?php
/**
 * @Author Mostafa Naguib
 * @Copyright Maximum Develop
 * @FileCreated 5/2/20 7:26 AM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */
namespace Olx\Tasks;

class TaskOne
{
    private $defaultFile = 'storage/words.txt';

    public function __construct(){
        echo 'Write a PHP or Python program to list all the anagrams in the given file.'.PHP_EOL;
    }

    public function cli(){
        echo "Enter the file path of your file, or leave it blank to use the provided one: ";
        $userEntry = getCommand();
        if($userEntry == ''){
            $this->printFileLinesForCli($this->defaultFile);
        } else {
            if(file_exists($userEntry)){
                $this->printFileLinesForCli($userEntry);
            } else {
                echo "File not exist, please make sure its full path not relative one";
                $this->cli();
            }
        }
    }

    public function web($file=null){
        $file = $file ?? $this->defaultFile;
        $this->printFileLinesForWeb($file);
    }

    public function iterateThroughFile($file){
        $fileHandler = fopen($file, 'r');
        while($line = fgets($fileHandler)){
            yield $line;
        }
        fclose($fileHandler);
    }

    public function printFileLinesForCli($file){
        foreach($this->iterateThroughFile($file) as $line){
            echo $line;
        }
    }

    public function printFileLinesForWeb($file){
        foreach($this->iterateThroughFile($file) as $line){
            echo $line.'<br>';
        }
    }
}
