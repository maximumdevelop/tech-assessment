<?php
/**
 * @Author Mostafa Naguib
 * @Copyright Maximum Develop
 * @FileCreated 5/2/20 7:40 AM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

namespace Olx\Tasks;


class TaskTwo
{
    private $pages;
    private $steps = 0;
    private $from;
    private $to;
    private $path = [];
    public function __construct()
    {
        $this->pages = [
            'page1'         =>      [
                'page2',
                'page3',
            ],
            'page2'         =>      [
                'page1'
            ],
            'page3'         =>      [
                'page4'
            ],
        ];
        echo "Consider the below mentioned links and try to write programs for the following.".PHP_EOL;
    }

    public function cli(){
        $availablePages = array_unique(flatten_array($this->pages));
        echo "Available distance are (".implode(',',$availablePages).")".PHP_EOL;
        echo "Enter the distance from: ";
        $userEntry = getCommand();
        if(!in_array($userEntry,$availablePages))
            $this->cli();
        $this->from = $userEntry;
        echo "Enter the distance to: ";
        $userEntry = getCommand();
        if(!in_array($userEntry,$availablePages))
            $this->cli();
        $this->to = $userEntry;

        $distance = $this->get_distance($this->from,$this->to);
        echo 'get_distance: '.$distance['get_distance'].PHP_EOL;
        echo 'list_of_pages: '.implode('->',$distance['list_of_pages']).PHP_EOL;
    }


    public function get_distance($from,$to){
        $this->from = $from;
        $this->to = $to;
        $this->find_parent();
        return [
            'get_distance'=>$this->steps,
            'list_of_pages' => $this->path,
        ];
    }

    public function find_parent() {
        foreach($this->pages as $key => $value){
            if($this->to == $key){
                array_unshift($this->path,$key);
                break;
            } else {
                $search = array_search($this->to,$value);
                if($search !== false){
                    array_unshift($this->path,$this->to);
                    $this->steps++;
                    $this->to = $key;
                    $this->find_parent();
                    break;
                }
            }
        }
    }
}
