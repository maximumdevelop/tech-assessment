<?php
/**
 * @Author Mostafa Naguib
 * @Copyright Maximum Develop
 * @FileCreated 5/2/20 8:43 AM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

namespace Olx\Tasks;


class TaskThree
{
    public function __construct()
    {
        echo "Given a function that returns a random integer number between 1 and 5, create a function that creates a random integer between 1 and 7. Use any language or write in pseudocode.".PHP_EOL;
    }

    public function cli(){
        echo "Get new Random number ? (y,n)";
        $userEntry = getCommand();
        if($userEntry == 'y'){
            echo $this->RandomOneToSeven().PHP_EOL;
            $this->cli();
        } else {
            echo 'Abort'.PHP_EOL;
        }
    }

    public function RandomOneToFive(){
        return rand(1,5);
    }

    public function RandomOneToSeven(){
        return $this->RandomOneToFive() + rand(0,2);
    }
}
