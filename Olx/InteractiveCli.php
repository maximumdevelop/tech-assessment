<?php
/**
 * @Author Mostafa Naguib
 * @Copyright Maximum Develop
 * @FileCreated 5/2/20 7:42 AM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

namespace Olx;


use Olx\Tasks\TaskOne;
use Olx\Tasks\TaskThree;
use Olx\Tasks\TaskTwo;

class InteractiveCli
{

    public function __construct()
    {
        echo 'Welcome to Interactive Cli'.PHP_EOL;
        echo 'please choose between tasks, (1,2 OR 3) :';
        switch(getCommand()){
            case 1:
                $this->TaskOne();
            break;
            case 2:
                $this->TaskTwo();
            break;
            case 3:
                $this->TaskThree();
            break;
            default:
                return new self();
        }
    }

    public function TaskOne(){
        $taskOne = new TaskOne();
        $taskOne->cli();
    }

    public function TaskTwo(){
        $taskTwo = new TaskTwo();
        $taskTwo->cli();
    }

    public function TaskThree(){
        $taskThree = new TaskThree();
        $taskThree->cli();
    }
}
