<?php
/**
 * @Author Mostafa Naguib
 * @Copyright Maximum Develop
 * @FileCreated 5/2/20 7:27 AM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */
require __DIR__.'/vendor/autoload.php';

if(php_sapi_name() === 'cli'){
    $interactiveCli = new \Olx\InteractiveCli();
    exit;
}

include_once __DIR__.'/views/layout/header.html';

$task = $_GET['task'];
switch ($task){
    case 'One':
        require __DIR__ . '/Olx/Controllers/Taskone.php';
    break;
    case 'Two':
        require __DIR__ . '/Olx/Controllers/Tasktwo.php';
    break;
    case 'Three':
        require __DIR__ . '/Olx/Controllers/Taskthree.php';
    break;
    default:
        require __DIR__ . '/views/index.html';
}

include_once __DIR__.'/views/layout/footer.html';
